<?php
# =========== Enable SVG media upload =========== #
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

# =========== ADD ACF OPTIONS =========== #
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

# =========== DISABLE REVISIONS =========== #
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

# =========== GFORMS - HIDE LABELS =========== #
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

# =========== MOVE YOAST TO BOTTOM =========== #
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

# =========== SOCIAL SHARE GENERATOR =========== #
function social_share($types, $post) {

    $url = urlencode(get_permalink($post->ID));
    $title = urlencode($post->post_title);
    $description = urlencode(get_the_excerpt($post));
    $image = get_the_post_thumbnail_url($post->ID, 'full');
    $js = "onclick='javascript:window.open(this.href,\" \", \"menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;\"";

    ?>

    <div class="social">
        <?php
            foreach($types as $t):
                switch($t):
                    case 'f': ?>
                        <a class="social-share__link social-share__link--facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?=$url;?>&amp;title=<?=$title;?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-facebook"></i></a>
                    <?php break;
                    case 't'; ?>
                        <a class="social-share__link social-share__link--twitter" href="http://twitter.com/intent/tweet?status=<?=$description." ".$url;?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-twitter"></i></a>
                    <?php break;
                    case 'p'; ?>
                        <a class="social-share__link social-share__link--pinterest" href="https://pinterest.com/pin/create/button/?url=<?=$url;?>&media=<?=$image;?>&description=<?=$description;?>" onclick="javascript:window.open(this.href,
                      '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=800,width=900');return false;"><i class="fa fa-pinterest"></i></a></a>
                    <?php break;

                    case 'g'; ?>
                        <a class="social-share__link social-share__link--googleplus" href="https://plus.google.com/share?url=<?=$url;?>" onclick="javascript:window.open(this.href,
                      '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-google-plus"></i></a>
                    <?php break;

                    case 'l'; ?>
                        <a class="social-share__link social-share__link--linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?=$url;?>&summary=<?=$description;?>" onclick="javascript:window.open(this.href,
                      '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=800,width=900');return false;"><i class="fa fa-linkedin"></i></a>
                    <?php break;

                endswitch;
            endforeach;
        ?>
    </div> <?php
}

function create_post_type() {
    register_post_type( 'Team Members',
        array(
            'labels' => array(
            'name' => __( 'Team Members' ),
            'singular_name' => __( 'Team Member' )
          ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'team-members'),
            'menu_icon'   => 'dashicons-groups',
            'supports' => array('title','editor','thumbnail')
        )
    );
}
add_action( 'init', 'create_post_type' );
