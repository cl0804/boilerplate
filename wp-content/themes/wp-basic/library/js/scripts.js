jQuery(document).ready(function($) {

	setTimeout(function() {
		$('body').addClass('loaded');
	}, 3000 );

	setTimeout(function() {
		$('body').addClass('complete');
	}, 3200 );

	new WOW().init();

	var navtoggle = $('.nav-toggle');

	navtoggle.on('click', function() {
		$('body').toggleClass('nav-open');
	});

	var nav = $('#menu-main-menu');
	var navLink = nav.find('.menu-item');
	var scrollDown = $('.masthead-button');

	navLink.on('click', function(e) {
			
		var anchor = $(this).find('a');
		var target = anchor.attr('href').split('#')[1];

		var div = $(document).find('#' + target);
		// console.log(target);
		// console.log('div: ' + div);

		if( div.length ) {
			e.preventDefault();
			smoothScroll( target, 150 );
		}
		
	});

	scrollDown.on('click', function() {
		smoothScroll('inner-content', 150);
	})

	function smoothScroll( target, offset ) {
		if (target) {
			// console.log(target);
	        if( $('body').hasClass('nav-open') ) {
	        	$('body').removeClass('nav-open');
	        }

	        target= $('#' + target);

	        // console.log('target: ' + target);

	        $('html, body').stop().animate({
	          scrollTop: target.offset().top -  offset
	        }, 1000);
	    }
	}

	/* =========== Testimonials Slider Init =========== */
		$('.testimonials__slider').owlCarousel({
		    loop:true,
		    margin:30,
		    nav: true,
		    items: 1,
		    dots: false,
		    autoplay: true,
		    smartSpeed: 900,
		    navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
		});


	/* =========== Back To Top =========== */
	var scrollTop = $("#BackToTop");

	$(window).scroll(function() {
	  var topPos = $(this).scrollTop();

	  if (topPos > 1500) {
	    $(scrollTop).css("opacity", "1");

	  } else {
	    $(scrollTop).css("opacity", "0");
	  }

	}); 

	//Click event to scroll to top
	$(scrollTop).click(function() {
	  $('html, body').animate({
	    scrollTop: 0
	  }, 800);
	  return false;

	});

	/* =========== Parallax Effect =========== */
	if (isIE()){
	   
	}else{
	    $(window).scroll(function() {
	    	var windowScroll = $(window).scrollTop();
	    	var windowHeight = $(window).height();
	    	var translate3d = windowScroll / 4;

	    	if( windowScroll < windowHeight ) {
	    		$('.image').css('transform', 'translate3d( 0px,' + translate3d + 'px, 0px)');
	    	}
	    });
	}
	

	function isIE() {
	  ua = navigator.userAgent;
	  /* MSIE used to detect old browsers and Trident used to newer ones*/
	  var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
	  
	  return is_ie; 
	}

	var nav = $('.header');
	var scrolled = false;
	var content = $('#main-content');
	var masthead = $('.masthead');
	var blurb = $('.box-blurb');
	
	/*Nav fixed when not on screen*/
	$(window).on('scroll', function() {

		var scrollTop = $(window).scrollTop();

		if( scrollTop > 150 ) {
			nav.addClass('fixed');
		} else {
			nav.removeClass('fixed');
		}

		if( scrollTop > 50 ) {
			masthead.addClass('inView');
			blurb.addClass('inView');
		} else {
			masthead.removeClass('inView');
			blurb.removeClass('inView');
		}
	})

	

	// $(window).scroll(function () {
	    
	//     if (300 < $(window).scrollTop() && !scrolled) {
	//         nav.addClass('fixed').animate({ top: '0px' });
	//         content.addClass('fixed');
	//         scrolled = true;
	//     }

	//    if (300 >= $(window).scrollTop() && scrolled) {
	//        //animates it out of view
	//        nav.animate({ top: '-150px' });  
	//        //sets it back to default style
	//         setTimeout(function(){
	//            nav.removeClass('fixed');
	          
	//         },500);
	//          content.removeClass('fixed');
	//         scrolled = false;      
	//     }

	//     console.log( $(window).scrollTop() )
 //    });

}); 
