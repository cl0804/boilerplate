<?php get_header(); ?>

	<div id="main-content">
		<?php include partial('masthead.php'); ?>
		<div id="inner-content" class="inner-content">
			<div class="grid">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="content">
						<?php the_content(); ?>
					</div>
				<?php endwhile; endif; ?>
				
				<?php get_testimonials(); ?>
			</div>
		</div>
	</div>


<?php get_footer(); ?>