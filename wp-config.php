<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-basic');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZWIHF.5ZL/0o]a7pCOVTO!LN!-wz*_`8U/%?_tW+X=6]qM)L{(i7r~!8p#F.+}]h');
define('SECURE_AUTH_KEY',  '_:obnq~)Y7xABs6D(<&As=J99h+,I5wLWfo7$mj3ivmqD0k:NB  CM5>JHi;fRXK');
define('LOGGED_IN_KEY',    '@bt^>cv?M_Cvh(~>RRoHT&bgWsv?C$mDMYI;K5,|RG!s3.(&%*[_%5|VM[~DdvRV');
define('NONCE_KEY',        '4p/;5.~oq0gGoGb|0%G}Hu_ kj?d1k[iE-/KgT%CKE%Rz]LfUVfh=5+I .B#K*2|');
define('AUTH_SALT',        '.~H0i(HH{/52Axn|HHh_YyLSlKVe;z#(!IufN94`.ASj0vlYvf>8ME%7JaRU=CD)');
define('SECURE_AUTH_SALT', 'Y,E/,zE?5kRX:PeM; #H~P~}`ZBi%$L<yU(l*lq7vq|e-HnE_I2PqtHP4<@:#SgK');
define('LOGGED_IN_SALT',   '{+vS:z*t?.w||JG?gh7rg/V?R {y2yDm$H:aHRe(CaO`Bla0T:1D&2HiwS3=&^{K');
define('NONCE_SALT',       '2%J:h</vw[eB7==BjyDYe]WEkI.>i:(%8D$[tim<nH9|MIKHAKBoT^;vQwOmHM{=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
